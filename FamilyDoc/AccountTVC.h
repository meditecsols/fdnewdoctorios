//
//  AccountTVC.h
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 12/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccountTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_balance;

@end

NS_ASSUME_NONNULL_END
