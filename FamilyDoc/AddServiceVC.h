//
//  AddServiceVC.h
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 27/07/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddServiceVC : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtFieldName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property NSMutableDictionary *dictService;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionBack:(id)sender;

@end
