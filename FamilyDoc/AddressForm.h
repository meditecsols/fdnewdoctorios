//
//  AddressForm.h
//  
//
//  Created by Martin Gonzalez on 17/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressForm : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
@property (strong,nonatomic) NSMutableDictionary *dictAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtTypeAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtStreet;
@property (strong, nonatomic) IBOutlet UITextField *txtZipCode;
@property (strong, nonatomic) IBOutlet UITextField *txtNeighbordHood;
@property (strong, nonatomic) IBOutlet UITextField *txtProvince;
@property (strong, nonatomic) IBOutlet UITextField *txtState;
@property (strong, nonatomic) IBOutlet UITextField *txtExtNumb;
@property (strong, nonatomic) IBOutlet UITextField *txtIntNumb;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UITextField *txtCountry;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property BOOL comeToEdit;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionSave:(id)sender;

@end
