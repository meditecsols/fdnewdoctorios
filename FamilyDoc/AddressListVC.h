//
//  AddressListVC.h
//  
//
//  Created by Martin Gonzalez on 17/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressListVC : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintTableHeight;
@property (strong, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintViewContainerHeight;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionAddAddress:(id)sender;
@end
