//
//  BackgroundGradient2.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 05/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "BackgroundGradient2.h"

@implementation BackgroundGradient2

- (void)drawRect:(CGRect)rect {
    // Drawing code
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00].CGColor, (id)[UIColor colorWithRed:0.090 green:0.133 blue:0.302 alpha:1.00].CGColor];
    
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
