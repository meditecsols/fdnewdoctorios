//
//  BackgroundGradientView.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 25/09/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "BackgroundGradientView.h"

@implementation BackgroundGradientView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    //gradient.startPoint = CGPointMake(0.0, 0.5);
    //gradient.endPoint = CGPointMake(1.0, 0.5);
    gradient.colors = @[(id)[UIColor colorWithRed:0.008 green:0.580 blue:0.710 alpha:1.00].CGColor, (id)[UIColor colorWithRed:0.039 green:0.498 blue:0.675 alpha:1.00].CGColor, (id)[UIColor colorWithRed:0.090 green:0.133 blue:0.302 alpha:1.00].CGColor];
    gradient.locations=@[@0.0,@0.1,@0.25];
    
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
