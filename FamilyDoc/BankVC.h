//
//  BankVC.h
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 12/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankVC : UIViewController<UIAdaptivePresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lbl_dinero_cuenta;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dinero_disponible;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dinero_liberar;
@property (weak, nonatomic) IBOutlet UILabel *lbl_clabe;
@property (weak, nonatomic) IBOutlet UITableView *tv_links;

@end

NS_ASSUME_NONNULL_END
