//
//  BankVC.m
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 12/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//


#import "BankVC.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "LinksTVC.h"



@interface BankVC ()
{
    NSMutableArray *links_array;
}

@end


@implementation BankVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tv_links.backgroundColor = UIColor.whiteColor;
    if([doctorInfo objectForKey:@"clabe"] != nil)
    {
        _lbl_clabe.text = [doctorInfo objectForKey:@"clabe"];
    }

}
- (IBAction)btn_back:(id)sender {
    [[NSNotificationCenter defaultCenter]
                       postNotificationName:@"BankDismissed"
                       object:nil userInfo:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btn_withdrawal:(id)sender {
    
    if ([[doctorInfo objectForKey:@"clabe"] length]<=0) {
       
       [self performSegueWithIdentifier:@"segue_register2" sender:nil];

    }
    else
    {
         [self performSegueWithIdentifier:@"segue_withdrawal" sender:nil];
    }
   
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self getAccounnt];
    [self getPendingWithdrawals];
    [self getPendingLinks];
   
}

- (void)presentationControllerDidDismiss:(UIPresentationController *)presentationController
{
     [self getAccounnt];
     [self getPendingWithdrawals];
     [self getPendingLinks];
    
}
-(void)getAccounnt
{
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getDoctorAccountData:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        
         dispatch_async(dispatch_get_main_queue(), ^{
          
              doctorAccountInfo =[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:responseData]];
              
               [[SharedFunctions sharedInstance] saveAccountData:doctorAccountInfo];
               
             self->_lbl_dinero_cuenta.text = [NSString stringWithFormat:@"$ %@",[doctorAccountInfo objectForKey:@"balance"]];
                    
        });
       
    }];
}
-(void)getPendingWithdrawals
{
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getPendingWithdrawalsDoctorData:[doctorInfo objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if(responseData.count >0)
              {
                  double total_amount = 0;
                  for(NSDictionary *dic in responseData)
                  {
                      NSString *status = [dic objectForKey:@"withdrawl_status"];
                      if([status isEqualToString:@"pending"])
                      {
                          NSString *amount_string = [dic objectForKey:@"amount"];
                          double amount_pending = [amount_string doubleValue];
                          total_amount = total_amount + amount_pending;
                          
                      }
                      
                  }
                  NSString *someString = [NSString stringWithFormat:@"$ %.2lf", total_amount];
                  self.lbl_dinero_liberar.text = someString;
                  
              }
              else
              {
                  self.lbl_dinero_liberar.text = @"$ 0.00";
              }
              
              
              
         });
        
    }];
    
}

-(void)getPendingLinks
{
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getLinkPaymentDataWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if(responseData.count >0)
              {

                  self->links_array = [NSMutableArray new];
                  for(NSDictionary *dic in responseData)
                  {
                      NSString *estatus = [dic objectForKey:@"estatus"];
                      if([estatus isEqualToString:@"pendiente"] || [estatus isEqualToString:@"order.pending_payment"])
                      {
                          [self->links_array addObject:dic];
                      }
                  }
                  if(self->links_array.count >0)
                  {
                      [self->_tv_links reloadData];
                      self->_tv_links.hidden = false;
                  }
                  else{
                      self->_tv_links.hidden = true;
                  }
            
                 
              }
              else
              {
                  self->_tv_links.hidden = true;
                  
              }
              
              
              
         });
        
    }];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: @"segue_withdrawal"]) {
        
        [[NSNotificationCenter defaultCenter]
        addObserver:self
        selector:@selector(didDismissWithdrawal)
        name:@"WithdrawalDismissed"
        object:nil];
        
        segue.destinationViewController.presentationController.delegate = self;
    }
}
- (void)dealloc
{
    // simply unsubscribe from *all* notifications upon being deallocated
    [[NSNotificationCenter defaultCenter] removeObserver:self];
} 
- (void)didDismissWithdrawal
{
    [self getAccounnt];
    [self getPendingWithdrawals];
    [self getPendingLinks];

}
- (IBAction)support_action:(id)sender {
    
    
     [[SharedFunctions sharedInstance] tappedOpenInfo];
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
       static NSString *simpleTableIdentifier = @"LinksTVC";
    
       LinksTVC *cell = (LinksTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
       if (cell == nil)
       {
           NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LinksTVC" owner:self options:nil];
           cell = [nib objectAtIndex:0];
       }
    
    NSDictionary *dic = [links_array objectAtIndex:indexPath.row];
    double amount = [[dic objectForKey:@"amount"] doubleValue];
    cell.lbl_amount.text = [self convertDoubleToCurrency:amount / 100];
    cell.lbl_name.text = [dic objectForKey:@"complete_name"];

    
    
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return links_array.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [links_array objectAtIndex:indexPath.row];
    NSString *nombre = [dic objectForKey:@"complete_name"];
    double amount = [[dic objectForKey:@"amount"] doubleValue];
    NSString *url = [dic objectForKey:@"url"];
    
    
    
    NSString *textToShare =[NSString stringWithFormat:@"%@, te envié una solicitud por %@ con FamilyDoc. Entra aquí y escoge tu método de pago: %@",nombre,[self convertDoubleToCurrency:amount/100],url];
    
    
    NSMutableArray *arr = [NSMutableArray new];
    [arr addObject:textToShare];
       UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:arr applicationActivities:nil];
       activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll]; //Exclude whichever aren't relevant
       [self presentViewController:activityVC animated:YES completion:nil];
    
}

-(NSString *) convertDoubleToCurrency:(double)amount{
          NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
          numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
          numberFormatter.locale = [NSLocale currentLocale];
    
          return [numberFormatter stringFromNumber:[NSNumber numberWithDouble:amount]];
}

@end
