//
//  CaptureSignVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 15/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface CaptureSignVC : UIViewController
@property BOOL comeFromMyProfile;
@property (nonatomic, weak) UIImage *image;
@property (nonatomic, strong) NSString *doctorName;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView3rdView;
- (IBAction)actionTakePhoto:(id)sender;
- (IBAction)actionClose:(id)sender;
- (IBAction)actionChangeSign:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblNameDoctor;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc1;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc2;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc3;
@property (strong, nonatomic) IBOutlet UIButton *btnNext1;
@property (strong, nonatomic) IBOutlet UIButton *btnChange;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;

- (IBAction)next1:(id)sender;
- (IBAction)saveSigning:(id)sender;
@end
