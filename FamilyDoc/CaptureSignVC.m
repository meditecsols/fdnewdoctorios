//
//  CaptureSignVC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 15/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "CaptureSignVC.h"
#import "TOCropViewController.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"

@interface CaptureSignVC ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate, TOCropViewControllerDelegate>{
    NSDictionary *userProfile;
    UIImage *imagePhotoSelected;
    BOOL comeFromCapture;
}
@property (nonatomic, strong) NSDictionary *metadata;
@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (nonatomic, assign) CGRect croppedFrame;
@property (nonatomic, assign) NSInteger angle;
@property(nonatomic,retain) UIView *helpView;
@end

@implementation CaptureSignVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _imageView=[[UIImageView alloc] init];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory=[documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/signing.png"]];
    NSData *imgData = [NSData dataWithContentsOfFile:documentsDirectory];
    UIImage *image = [[UIImage alloc] initWithData:imgData];
    
    
    //NSFileManager *fileMgr = [NSFileManager defaultManager];
    if ([[doctorInfo objectForKey:@"signature"] length]>0){
        _helpView=[[UIView alloc] init];
        _helpView=[[[NSBundle mainBundle] loadNibNamed:@"CaptureSign3" owner:self options:nil] objectAtIndex:0];
        [_helpView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
        [self.view addSubview:_helpView];
        if (!_comeFromMyProfile) {
            [self.imageView3rdView setImage:image];
            _lblNameDoctor.text=[NSString stringWithFormat:@"%@ %@ %@" ,[doctorInfo objectForKey:@"first_name"],[doctorInfo objectForKey:@"last_name"],[doctorInfo objectForKey:@"second_last_name"]];
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke getImageFromUrl:[doctorInfo objectForKey:@"signature"] WithCompletion:^(UIImage * _Nullable imageDownloaded, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!error) {
                        [self->_imageView3rdView setImage:[self imageWithImage:imageDownloaded scaledToWidth:self->_imageView3rdView.frame.size.width scaledToHeight:self->_imageView3rdView.frame.size.height]];
                    }
                    
                });
                
            }];
        }else{
            _lblNameDoctor.text=[NSString stringWithFormat:@"%@ %@ %@" ,[doctorInfo objectForKey:@"first_name"],[doctorInfo objectForKey:@"last_name"],[doctorInfo objectForKey:@"second_last_name"]];
        }
        
        
    }
    
    NSString *text =[NSString stringWithFormat:@"Haz una firma grande, "];
    NSString *text2=[NSString stringWithFormat:@"del tamaño\nde la pantalla de tu celular en\nuna hoja blanca."];
    NSString *text3=[NSString stringWithFormat:@"%@%@",text,text2];
    NSMutableAttributedString *attributedText =[[NSMutableAttributedString alloc] initWithString:text3];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Bold" size:17] range:[text3 rangeOfString:text]];
    NSRange blackTextRange = [text3 rangeOfString:text2];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:17] range:blackTextRange];
    _lblDesc1.attributedText=attributedText;
    
    text =[NSString stringWithFormat:@"Tómale foto "];
    text2=[NSString stringWithFormat:@"cuidando que no\nsalgan sombras cerca de la firma."];
    text3=[NSString stringWithFormat:@"%@%@",text,text2];
    attributedText =[[NSMutableAttributedString alloc] initWithString:text3];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Bold" size:17] range:[text3 rangeOfString:text]];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:17] range:[text3 rangeOfString:text2]];
    _lblDesc2.attributedText=attributedText;
    
    text =[NSString stringWithFormat:@"Ajusta los bordes "];
    text2=[NSString stringWithFormat:@"para\nperfeccionar la captura."];
    text3=[NSString stringWithFormat:@"%@%@",text,text2];
    attributedText =[[NSMutableAttributedString alloc] initWithString:text3];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Bold" size:17] range:[text3 rangeOfString:text]];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:17] range:[text3 rangeOfString:text2]];
    _lblDesc3.attributedText=attributedText;
    
    _btnChange.layer.borderColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00].CGColor;
    if (_comeFromMyProfile) {
        if ([[doctorInfo objectForKey:@"signature"] length]>0) {
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke getImageFromUrl:[doctorInfo objectForKey:@"signature"] WithCompletion:^(UIImage * _Nullable imageDownloaded, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!error) {
                        [self->_imageView3rdView setImage:[self imageWithImage:imageDownloaded scaledToWidth:self->_imageView3rdView.frame.size.width scaledToHeight:self->_imageView3rdView.frame.size.height]];
                    }
                    
                });
                
            }];
        }
        
        
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!comeFromCapture) {
        if ([NSString stringWithFormat:@"%@",userProfile[@"signature"]].length<=0) {
            _helpView=[[UIView alloc] init];
            _helpView=[[[NSBundle mainBundle] loadNibNamed:@"SignHelpView1" owner:self options:nil] objectAtIndex:0];
            [_helpView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
            [self.view addSubview:_helpView];
        }
    }
    
}
#pragma mark - Image Picker Delegate -
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    imagePhotoSelected=[[UIImage alloc] init];
    imagePhotoSelected=image;
    comeFromCapture=YES;
    _helpView=[[UIView alloc] init];
    _helpView=[[[NSBundle mainBundle] loadNibNamed:@"CaptureSign2" owner:self options:nil] objectAtIndex:0];
    [_helpView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
    [self.view addSubview:_helpView];
    
    [picker dismissViewControllerAnimated:YES completion:nil];

    
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Gesture Recognizer -
- (void)didTapImageView{
    //When tapping the image view, restore the image to the previous cropping state
    
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:self.image];
    cropController.delegate = self;
    CGRect viewFrame = [self.view convertRect:self.imageView.frame toView:self.navigationController.view];
    [cropController presentAnimatedFromParentViewController:self
                                                  fromImage:self.imageView.image
                                                   fromView:nil
                                                  fromFrame:viewFrame
                                                      angle:self.angle
                                               toImageFrame:self.croppedFrame
                                                      setup:^{ self.imageView.hidden = YES; }
                                                 completion:nil];
}

#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle{
    self.croppedFrame = cropRect;
    self.angle = angle;
    [self updateImageViewWithImage:[self removeOrientationProperty:image] fromCropViewController:cropViewController];
    //[self saveSigningToApi];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle{
    self.croppedFrame = cropRect;
    self.angle = angle;
    [self updateImageViewWithImage:[self removeOrientationProperty:image] fromCropViewController:cropViewController];
}
- (UIImage*)removeOrientationProperty:(UIImage*)image {
    CGSize size = image.size;
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0,0,size.width ,size.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController{
    self.imageView.image =[self imageWithImage:[self createMask:[self changeWhiteColorTransparent:[self convertOriginalImageToBWImage:image]]] scaledToWidth:1000 scaledToHeight:700];
    NSString *nameImage=[NSString stringWithFormat:@"signing.png"];
    
    NSString *path =
    [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
     stringByAppendingPathComponent:nameImage];
    assert(path);
    NSData *data = UIImagePNGRepresentation(self.imageView.image);
    NSURL *url = [NSURL fileURLWithPath:path];
    [data writeToURL:url atomically:YES];
    
    // [self layoutImageView];
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
    
    if (cropViewController.croppingStyle != TOCropViewCroppingStyleCircular) {
        //self.imageView.hidden = YES;
        _helpView=[[UIView alloc] init];
        _helpView=[[[NSBundle mainBundle] loadNibNamed:@"CaptureSign3" owner:self options:nil] objectAtIndex:0];
        [_helpView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
        _btnChange.layer.borderColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00].CGColor;

        [cropViewController dismissAnimatedFromParentViewController:self
                                                   withCroppedImage:image
                                                             toView:self.imageView
                                                            toFrame:CGRectZero
                                                              setup:^{ }
                                                         completion:
         ^{
            
             
            [self.view addSubview:self->_helpView];
                        [self.imageView3rdView setImage:self->_imageView.image];
                        self->_lblNameDoctor.text=self->_doctorName;
             
             
             
         }];
    }
    else {
        self.imageView.hidden = NO;
        [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Family Doc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];

    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)cropViewController:(nonnull TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled{
    comeFromCapture=NO;
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tappedCancel:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width scaledToHeight:(float) i_height
{
    UIImage *newImage;
    if (sourceImage.size.width>i_width) {
        float oldWidth = sourceImage.size.width;
        float scaleFactor = i_width / oldWidth;
        
        float newHeight = sourceImage.size.height * scaleFactor;
        float newWidth = oldWidth * scaleFactor;
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else{
        newImage=sourceImage;
    }
    
    if(newImage.size.height>i_height){
        float oldHeight = sourceImage.size.height;
        float scaleFactor = i_height / oldHeight;
        
        float newWidth = sourceImage.size.width * scaleFactor;
        float newHeight =oldHeight * scaleFactor;
        
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return newImage;
}
-(UIImage *)createMask:(UIImage*)image
{
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, image.scale);
    CGContextRef c = UIGraphicsGetCurrentContext();
    [image drawInRect:rect];
    CGContextSetFillColorWithColor(c, [[UIColor colorWithRed:0.157 green:0.259 blue:0.514 alpha:1.00] CGColor]);
    CGContextSetBlendMode(c, kCGBlendModeSourceAtop);
    CGContextFillRect(c, rect);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}
-(UIImage *)changeWhiteColorTransparent: (UIImage *)image
{
    //convert to uncompressed jpg to remove any alpha channels
    //this is a necessary first step when processing images that already have transparency
    image = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1.0)];
    CGImageRef rawImageRef=image.CGImage;
    //RGB color range to mask (make transparent)  R-Low, R-High, G-Low, G-High, B-Low, B-High
    const CGFloat colorMasking[6] = {152, 255, 152, 255, 152, 255};
    
    UIGraphicsBeginImageContext(image.size);
    CGImageRef maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    
    //iPhone translation
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height), maskedImageRef);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    return result;
}
-(UIImage *)convertOriginalImageToBWImage:(UIImage *)originalImage
{
    CIImage *beginImage = [[CIImage alloc] initWithCGImage:originalImage.CGImage];
    
    CIImage *blackAndWhite = [CIFilter filterWithName:@"CIColorControls" keysAndValues:kCIInputImageKey, beginImage, @"inputBrightness", [NSNumber numberWithFloat:0.0], @"inputContrast", [NSNumber numberWithFloat:1.1], @"inputSaturation", [NSNumber numberWithFloat:0.0], nil].outputImage;
    CIImage *output = [CIFilter filterWithName:@"CIExposureAdjust" keysAndValues:kCIInputImageKey, blackAndWhite, @"inputEV", [NSNumber numberWithFloat:0.7], nil].outputImage;
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgiimage = [context createCGImage:output fromRect:output.extent];
    //UIImage *newImage = [UIImage imageWithCGImage:cgiimage];
    UIImage *newImage = [UIImage imageWithCGImage:cgiimage scale:originalImage.scale orientation:originalImage.imageOrientation];
    CGImageRelease(cgiimage);
    
    return newImage;
}
-(UIImage *)convertImageToSquare:(UIImage *)originalImage{
    
    CGFloat width = 1000;
    CGFloat height = 1000;
    
    if (originalImage.size.width>originalImage.size.height) {
        width=originalImage.size.width;
        height=originalImage.size.width;
    }else if (originalImage.size.height>originalImage.size.width){
        width=originalImage.size.height;
        height=originalImage.size.height;
    }else{
        return originalImage;
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    
    // Now we can draw anything we want into this new context.
    CGPoint origin = CGPointMake((width - originalImage.size.width) / 2.0f,
                                 (height - originalImage.size.height) / 2.0f);
    [originalImage drawAtPoint:origin];
    
    // Clean up and get the new image.
    UIGraphicsPopContext();
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)actionTakePhoto:(id)sender {
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    
    [self presentViewController:standardPicker animated:YES completion:^{
        //[_helpView removeFromSuperview];
    }];
}

- (IBAction)actionClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionChangeSign:(id)sender {
    [_helpView removeFromSuperview];
}

- (IBAction)next1:(id)sender {
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imagePhotoSelected];
    cropController.delegate = self;
    self.image = imagePhotoSelected;
    
    cropController.modalPresentationStyle= UIModalPresentationFullScreen;
    [self presentViewController:cropController animated:YES completion:^{
        [self->_helpView removeFromSuperview];
    }];

}
- (IBAction)saveSigning:(id)sender{
        InvokeService *invoke3=[[InvokeService alloc] init];
        NSMutableDictionary *dictToChange=[[NSMutableDictionary alloc] init];
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        //documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        documentsDirectory=[documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/signing.png"]];
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        //fileMgr = [NSFileManager defaultManager];
        if ([fileMgr fileExistsAtPath:documentsDirectory]){
            [dictToChange setObject:documentsDirectory forKey:@"pathSignatureImage"];
        }
        
        [[SharedFunctions sharedInstance] showLoadingView];
        [invoke3 updateDoctorWithData:dictToChange WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                if ([[responseData allKeys] count]==0) {
                    [self showAlertWithMessage:@"Error al actualizar sus datos"];
                }else{
                    doctorInfo=[[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:responseData] mutableCopy];
                    [self showAlertWithMessage:@"Tu firma se ha almacenado"];
                }
                
            });
            
        }];

}
@end
