//
//  ChatClientManager.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 2/22/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatClientManager : NSObject
+(ChatClientManager *) sharedInstance;
@end

NS_ASSUME_NONNULL_END
