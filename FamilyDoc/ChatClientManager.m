//
//  ChatClientManager.m
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 2/22/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import "ChatClientManager.h"

@implementation ChatClientManager
+(ChatClientManager *)sharedInstance{
    
    static ChatClientManager *_sharedInstance =  nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[ChatClientManager alloc] init];
    });
    
    return _sharedInstance;
}
@end
