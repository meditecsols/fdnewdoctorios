//
//  ChatDifusionVC.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 6/27/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import "ChatVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatDifusionVC : ChatVC
@property (nonatomic, strong) NSMutableArray *patientsOfCompany;
@property (nonatomic, strong) NSDictionary *companySelected;
@property (strong, nonatomic) NSMutableArray *arrayOfChannels;
@end

NS_ASSUME_NONNULL_END
