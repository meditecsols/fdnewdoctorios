//
//  ChatDifusionVC.m
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 6/27/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import "ChatDifusionVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Config.h"
#import "GlobalMembers.h"
@interface ChatDifusionVC ()
@property (strong, nonatomic) NSArray<SBDBaseMessage *> *dumpedMessages;
@property (strong, nonatomic) NSMutableArray<SBDBaseMessage *> *messages;
@property (strong, nonatomic) NSMutableDictionary<NSString *, SBDBaseMessage *> *resendableMessages;
@property (strong, nonatomic) NSMutableDictionary<NSString *, SBDBaseMessage *> *preSendMessages;
@property (strong, nonatomic) NSMutableDictionary<NSString *, NSDictionary<NSString *, NSObject *> *> *resendableFileData;
@property (strong, nonatomic) NSMutableDictionary<NSString *, NSDictionary<NSString *, NSObject *> *> *preSendFileData;
@end

@implementation ChatDifusionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _arrayOfChannels=[[NSMutableArray alloc] init];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"%@",[_patientsOfCompany description]);
    self.consultationButton.hidden=YES;
    [self connectSendBird];
}
-(void)setNavigationBarTitleViewWithName:(NSString *)name{
    
    UIView *whole = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 100, 100)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, 40, 40)];
    
    // [imageView.layer setCornerRadius:25.0f];
    [imageView setClipsToBounds:YES];
    [imageView setContentMode:UIViewContentModeScaleToFill];
    
    
    [imageView setImage:[UIImage imageNamed:@"imgTestProfile"]];

    if ([[_companySelected objectForKey:@"company_pic"] length]>0) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:[_companySelected objectForKey:@"company_pic"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (image) {
                [self roundImage:imageView];
            }
            else{
                [imageView setImage:[UIImage imageNamed:@"imgTestProfile"]];
            }
            
        }];
    }
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 15,whole.frame.size.width-60, 15)];
    [nameLabel setText:[_companySelected objectForKey:@"name"]];
    [nameLabel setTextColor:[UIColor colorWithRed:0.525 green:0.525 blue:0.525 alpha:1.00]];
    [nameLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:15]];
    
    //[whole addSubview:titleLabel];
    [whole addSubview:nameLabel];
    [whole addSubview:imageView];
    
    self.navigationItem.titleView = whole;
}
-(void)roundImage:(UIImageView *)imgProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 0.5f;
    imgProfile.contentMode=UIViewContentModeScaleAspectFill;
    imgProfile.layer.borderColor = [UIColor grayColor].CGColor;
    imgProfile.clipsToBounds = YES;
}
-(void)connectSendBird{
    NSString *idStringDr=[NSString stringWithFormat:@"%d",[[doctorInfo objectForKey:@"id"] intValue] ];
    if ([_patientsOfCompany count]>0) {
        NSString *idStringPatient=[NSString stringWithFormat:@"%d",[[[_patientsOfCompany objectAtIndex:0] objectForKey:@"id"] intValue] ];
        [SBDGroupChannel createChannelWithUserIds:@[idStringDr,idStringPatient] isDistinct:YES completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {
            if (error != nil) { // Error.
                return;
            }
            
            if ([channel.members count]<=1) {
                [self->_patientsOfCompany removeObjectAtIndex:0];
                [self connectSendBird];
                return;
            }
            NSLog(@"channel created %@",channel.channelUrl);
            for (SBDMember *miembro in channel.members) {
                NSLog(@"members of channel: %@",miembro.userId);
            }
            
            [SBDGroupChannel getChannelWithUrl:channel.channelUrl completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {
                if (error != nil) { // Error.
                    return;
                }
                [self->_arrayOfChannels addObject:channel];
                [self->_patientsOfCompany removeObjectAtIndex:0];
                [self connectSendBird];
            }];
        }];
    }
    
//
//
//    for (NSDictionary *dictPatient in _patientsOfCompany) {
//        NSString *idStringPatient=[NSString stringWithFormat:@"%d",[[dictPatient objectForKey:@"id"] intValue] ];
//        NSLog(@"Patient %@",idStringPatient);
//        [SBDGroupChannel createChannelWithUserIds:@[idStringDr,idStringPatient] isDistinct:YES completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {
//            if (error != nil) { // Error.
//                return;
//            }
//            for (SBDMember *miembro in channel.members) {
//                NSLog(@"members of channel:%@ %@",channel.channelUrl,miembro.userId);
//            }
//
//            NSLog(@"channel created %@",channel.channelUrl);
//            [SBDGroupChannel getChannelWithUrl:channel.channelUrl completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {
//                if (error != nil) { // Error.
//                    return;
//                }
//                [self->_arrayOfChannels addObject:channel];
//            }];
//        }];
//    }
    
}
-(void)sendMessage:(NSString *)message{
    if (message > 0) {
        for (SBDGroupChannel *channelSelected in _arrayOfChannels) {
            NSLog(@"chnnel idnetifier %@",channelSelected.channelUrl);
            [channelSelected sendUserMessage:message data:@"" customType:@""  completionHandler:^(SBDUserMessage * _Nullable userMessage, SBDError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (error != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self processMessage:(SBDBaseMessage *)userMessage];
                            [self finishReceivingMessage];
                        });
                        
                        return;
                    }
                });
            }];
       
        }
        }
        
}
@end
