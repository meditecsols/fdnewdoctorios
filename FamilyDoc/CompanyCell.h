//
//  CompanyCell.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 6/27/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CompanyCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;
@property (strong, nonatomic) IBOutlet UIImageView *imgCompany;
@end

NS_ASSUME_NONNULL_END
