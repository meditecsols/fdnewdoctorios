//
//  ConsultasVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 18/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ConsultasVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintTopHeader;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControlView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedConsultationSelected;
@property (strong, nonatomic) IBOutlet UIWebView *myWebView;
- (IBAction)actionChangeTypeConsultation:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *patientsTV;
- (IBAction)actionOpenQR:(id)sender;
- (IBAction)actionOpenMenu:(id)sender;
- (IBAction)actionOpenPacientes:(id)sender;
- (IBAction)actionOpenInfo:(id)sender;
@end
