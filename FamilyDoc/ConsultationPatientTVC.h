//
//  ConsultationPatientTVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 22/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsultationPatientTVC : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgPatient;
@property (strong, nonatomic) IBOutlet UILabel *lblNamePatient;
@property (strong, nonatomic) IBOutlet UILabel *lblDiagnose;
@property (strong, nonatomic) IBOutlet UILabel *lblSuffering;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UIButton *btnPill;
@property (strong, nonatomic) IBOutlet UIButton *btnNotes;
@end
