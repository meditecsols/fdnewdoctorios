//
//  ConsultationPatientTVC2.h
//  
//
//  Created by Martin Gonzalez on 01/03/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsultationPatientTVC2 : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgPatient;
@property (strong, nonatomic) IBOutlet UILabel *lblNamePatient;
@property (strong, nonatomic) IBOutlet UILabel *lblDiagnose;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblHour;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UIButton *btnPill;
@property (strong, nonatomic) IBOutlet UIButton *btnNotes;
@end
