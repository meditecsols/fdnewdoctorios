//
//  DifusionListVC.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 6/27/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DifusionListVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *companiesTableView;
- (IBAction)actionBack:(id)sender;
@end

NS_ASSUME_NONNULL_END
