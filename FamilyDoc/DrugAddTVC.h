//
//  DrugAddTVC.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 13/09/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrugAddTVC : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;
@property (strong, nonatomic) IBOutlet UIButton *btnAddDrug;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintLeftMargin;
@end
