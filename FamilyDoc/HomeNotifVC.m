//
//  HomeNotifVC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 24/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "HomeNotifVC.h"
#import "NotifTVC.h"

@interface HomeNotifVC ()

@end

@implementation HomeNotifVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    UISwipeGestureRecognizer *swipeTop = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
//    swipeTop.direction = UISwipeGestureRecognizerDirectionUp;
//    [self.view addGestureRecognizer:swipeTop];
//
//    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
//    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
//    [self.view addGestureRecognizer:swipeDown];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    _NotifTV.delegate=self;
    _NotifTV.dataSource=self;
    [_NotifTV reloadData];
    self.NotifTV.allowsMultipleSelectionDuringEditing = NO;
}
- (void)swipe:(UISwipeGestureRecognizer *)swipeRecogniser
{
    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionRight)
    {
        UIStoryboard *storyboard = self.storyboard;
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeAjustesVC"];
        
        
        [self presentViewController:viewController animated:NO completion:^{
        }];
    }
    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionLeft)
    {
        
        
        UIStoryboard *storyboard = self.storyboard;
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomePacientesVC"];
        
        
        [self presentViewController:viewController animated:NO completion:^{
        }];
    }
    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionUp)
    {
        _layoutConstraintTopHeader.constant=-76;
        [self.view layoutIfNeeded];
    }
    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionDown)
    {
        _layoutConstraintTopHeader.constant=0;
        [self.view layoutIfNeeded];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGRect frame = tableView.frame;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width,40)];
    [headerView setBackgroundColor:[UIColor colorWithRed:0.961 green:0.953 blue:0.961 alpha:1.00]];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, tableView.frame.size.width, 20)];
    title.textAlignment=NSTextAlignmentCenter;
    title.textColor=[UIColor colorWithRed:0.592 green:0.592 blue:0.592 alpha:1.00];
    title.font=[UIFont fontWithName:@"MyriadPro-Semibold" size:15];
    switch (section) {
        case 0:
            title.text =@"HOY";
            [headerView setBackgroundColor:[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00]];
            title.textColor=[UIColor whiteColor];
            break;
        case 1:
            title.text =@"JULIO";
            break;
        case 2:
            title.text =@"JUNIO";
            break;
        default:
            break;
    }
    
    
    [headerView addSubview:title];
    return headerView;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 66;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTab = @"NotifTVC";
    NotifTVC *cell = (NotifTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTab];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTab owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (indexPath.row==19) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell2==nil){
            cell2=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            //cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"campo1.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
            
        }
        [cell2 setBackgroundColor:[UIColor whiteColor]];
        return cell2;
        
    }
    return cell;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
    }
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        //insert your editAction here
    }];
    editAction.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"notifAceptAction"]];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@""  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        //insert your deleteAction here
    }];
    deleteAction.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"notifCancelAction"]];
    return @[deleteAction,editAction];
}

- (IBAction)actionOpenQR:(id)sender {
}
@end
