//
//  HomePacientesVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 19/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePacientesVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnInvitePatients;
@property (strong, nonatomic) IBOutlet UIButton *btnDifusionList;
@property (strong, nonatomic) IBOutlet UIView *viewLineSeparator;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintTopHeader;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightButtonInvite;

@property (strong, nonatomic) IBOutlet UISearchBar *patientSearch;
@property (strong, nonatomic) IBOutlet UITableView *patientsTableView;
 @property (nonatomic, strong) NSMutableArray *searchResult;
- (IBAction)actionOpenQR:(id)sender;
- (IBAction)actionOpenMenu:(id)sender;
- (IBAction)actionOpenConsultas:(id)sender;
- (IBAction)actionOpenInfo:(id)sender;
- (IBAction)actionInvitePatients:(id)sender;
-(void)openChatWithPatientId:(int)patientId;
-(void)openChatWithPatientName:(NSString *)patientName;
@end
