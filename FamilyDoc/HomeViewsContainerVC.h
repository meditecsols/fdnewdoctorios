//
//  HomeViewsContainerVC.h
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 06/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewsContainerVC : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintWidthContainer;
@property (strong, nonatomic) IBOutlet UIView *viewHome1;
@property (strong, nonatomic) IBOutlet UIView *viewHome2;
@property (strong, nonatomic) IBOutlet UIView *viewHome3;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollContainer;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnPacientes;
@property (strong, nonatomic) IBOutlet UIButton *btnConsultas;

-(void)moveToFirstView;
-(void)moveToSecondView;
-(void)moveToThirdView;
- (IBAction)actionOpenMenu:(id)sender;
- (IBAction)actionOpenPacientes:(id)sender;
- (IBAction)actionOpenConsultas:(id)sender;
@end
