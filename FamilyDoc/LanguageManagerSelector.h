//
//  LanguageManagerSelector.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 17/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageManagerSelector : NSObject

@property (strong,nonatomic) NSArray *languageCollection;

+(NSString*)languageSelectedStringForKey:(NSString*)key;

@end
