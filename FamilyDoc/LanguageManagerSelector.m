//
//  LanguageManagerSelector.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 17/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "LanguageManagerSelector.h"

@implementation LanguageManagerSelector
@synthesize languageCollection;

-(id)init{
    
    if (self = [super init])
    {
        
        languageCollection = [[NSArray alloc] initWithObjects:@"es",@"en",@"es-MX",nil];
    }
    return self;
}



+(NSString*)languageSelectedStringForKey:(NSString*)key

{
    
    NSString *lan = @"es-MX";
    
    NSString *path;
    
    NSBundle *languageBundle;
    
    
    NSString* str;
    
    path = [[NSBundle mainBundle] pathForResource:lan ofType:@"lproj"];
    
    languageBundle = [NSBundle bundleWithPath:path];
    str=[languageBundle localizedStringForKey:key value:@"" table:nil];
    
    return str;
    
}


@end
