//
//  LinkPayBankVC.h
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 24/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LinkPayBankVC : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txt_complete_name;
@property (weak, nonatomic) IBOutlet UITextField *txt_email;
@property (weak, nonatomic) IBOutlet UITextField *txt_phone;
@property (weak, nonatomic) IBOutlet UITextField *txtTotalCost;
@property (weak, nonatomic) IBOutlet UILabel *lblNegativeAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblComisionMeditec;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalLessComissions;

@end

NS_ASSUME_NONNULL_END
