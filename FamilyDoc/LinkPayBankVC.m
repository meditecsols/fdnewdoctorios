//
//  LinkPayBankVC.m
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 24/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import "LinkPayBankVC.h"
#import "GlobalMembers.h"
#import "InvokeService.h"
#import "SharedFunctions.h"

@interface LinkPayBankVC ()
{
    NSNumber *myNumber;
}
@end

@implementation LinkPayBankVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

   
    _txt_email.delegate = self;
    _txt_complete_name.delegate = self;
    _txt_phone.delegate = self;
    _txtTotalCost.delegate=self;
    // Do any additional setup after loading the view.
}
- (IBAction)back_action:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == self.txt_complete_name)
    {
        /*  limit to only numeric characters  */
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if ([myCharSet characterIsMember:c]) {
                return NO;
            }
        }

        /*  limit the users input to only 9 characters  */
        NSUInteger newLength = [self->_txt_complete_name.text length] + [string length] - range.length;
        return (newLength > 100) ? NO : YES;
    }
    if(textField == self.txt_phone)
    {
        if (textField.text.length < 10 || string.length == 0)
        {
            return YES;
        }
        else
        {
            return NO;
        }
   }
   if ([textField isEqual:_txtTotalCost]) {
       NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
       NSInteger centValue = [cleanCentString intValue];
       NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
       myNumber = [f numberFromString:cleanCentString];
       NSNumber *result;
       
       if([textField.text length] < 16){
           if (string.length > 0)
           {
               centValue = centValue * 10 + [string intValue];
               double intermediate = [myNumber doubleValue] * 10 +  [[f numberFromString:string] doubleValue];
               result = [[NSNumber alloc] initWithDouble:intermediate];
           }
           else
           {
               centValue = centValue / 10;
               double intermediate = [myNumber doubleValue]/10;
               result = [[NSNumber alloc] initWithDouble:intermediate];
           }
           
           myNumber = result;
           NSLog(@"%ld ++++ %@", (long)centValue, myNumber);
           NSNumber *formatedValue;
           formatedValue = [[NSNumber alloc] initWithDouble:[myNumber doubleValue]/ 100.0f];
           
           if(formatedValue.doubleValue < 5000.01){
               NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
               [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
               textField.text = [_currencyFormatter stringFromNumber:formatedValue];
               if([textField isEqual:_txtTotalCost]){
                   [self calculateTotalAmountWithoutServices];
               }
           }else{
               [UIView animateWithDuration:0.1 animations:^(void){
                   [textField resignFirstResponder];
               } completion:^(BOOL finished) {
                   [self showAlertWithMessage:@"El monto máximo es de $5,000 pesos"];
               }];
               return NO;
               
               
           }
           return NO;
       }else{
           
           NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
           [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
           textField.text = [_currencyFormatter stringFromNumber:@0];
           
           return NO;
       }
   }
   else{
       return YES;
   }
     return YES;
}

-(void)calculateTotalAmountWithoutServices{
    float consultPrice=[self getFloatFromPriceText:_txtTotalCost.text];
    
    _txtTotalCost.text=[self getFormatPriceFromFloat:consultPrice];
    float meditecComission=11.6;
    float conektaTax=(((consultPrice/100)*2.9)+2.5)*1.16;
    float totalTax=meditecComission+conektaTax;
    _lblComisionMeditec.text=[NSString stringWithFormat:@"- %@",[self getFormatPriceFromFloat:totalTax]];
    float totalLessComissions=consultPrice-meditecComission-conektaTax;
    _lblTotalLessComissions.text=[self getFormatPriceFromFloat:totalLessComissions];
    if (totalLessComissions<0) {
        _lblTotalLessComissions.textColor=[UIColor colorWithRed:0.714 green:0.204 blue:0.149 alpha:1.00];
        _txtTotalCost.textColor=[UIColor colorWithRed:0.714 green:0.204 blue:0.149 alpha:1.00];
        _lblNegativeAmount.hidden=NO;
      
    }
    else{
        _lblTotalLessComissions.textColor=[UIColor colorWithRed:0.000 green:0.796 blue:0.980 alpha:1.00];
        _txtTotalCost.textColor=[UIColor colorWithRed:0.000 green:0.796 blue:0.980 alpha:1.00];
        _lblNegativeAmount.hidden=YES;
      
        
    }
    if (consultPrice==0) {
        _lblComisionMeditec.text=[NSString stringWithFormat:@"- %@",[self getFormatPriceFromFloat:0]];
        _lblTotalLessComissions.text=[self getFormatPriceFromFloat:0];
        _lblTotalLessComissions.textColor=[UIColor colorWithRed:0.000 green:0.796 blue:0.980 alpha:1.00];
    }
    
}
-(float)getFloatFromPriceText:(NSString *)priceText{
    float amount=0;
    amount=[[[priceText substringFromIndex:1] stringByReplacingOccurrencesOfString:@"," withString:@""] floatValue];
    return amount;
}
-(NSString *)getFormatPriceFromFloat:(float)priceFloat{
    NSNumber *formatedValue;
    formatedValue = [[NSNumber alloc] initWithFloat:priceFloat];
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [_currencyFormatter stringFromNumber:formatedValue];
    //return [NSString stringWithFormat:@"$%.02f", priceFloat];;
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Family Doc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];

    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)generate
{
    
    double total = [self convertCurrencyToDouble:_txtTotalCost.text];
    total = total * 100;
    
    if(total <= 2000)
    {
        [self showAlertWithMessage:@"Ingresa una cantidad mayor a $20 pesos"];
        return;
    }
    NSNumber *total_Number = [[NSNumber alloc] initWithDouble:total];
    NSMutableDictionary *dic = [NSMutableDictionary new];
    NSString *id_doc = [NSString stringWithFormat:@"FD-%@",[doctorInfo objectForKey:@"id"]];
    NSString *patient = _txt_complete_name.text;
    NSString *email = [_txt_email.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [dic setValue:id_doc forKey:@"id"];
    [dic setValue:total_Number forKey:@"amount"];
    [dic setValue:patient forKey:@"name"];
    [dic setValue:email forKey:@"email"];
    [dic setValue:_txt_phone.text forKey:@"phone"];
    [dic setValue:[NSNumber numberWithBool:false] forKey:@"is_fd_user"];

   
    
    InvokeService *inv = [InvokeService new];
    [[SharedFunctions sharedInstance] showLoadingView];
    [inv createLinkPayWithInfo:dic WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if( responseData != nil)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                [[NSNotificationCenter defaultCenter]
                postNotificationName:@"WithdrawalDismissed"
                object:nil userInfo:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            });
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                [self showAlertWithMessage:@"Hubo un error, verifica los datos y vuelve a intentar"];
            });
            
        }
       

    }];
    
}
-(double) convertCurrencyToDouble:(NSString *)input {
     
     NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    numberFormatter.locale = [NSLocale currentLocale];
    
    return [[numberFormatter numberFromString:input] doubleValue];
}
- (IBAction)generate_action:(id)sender {
    
    if(_txt_complete_name.text.length == 0)
    {
        [self showAlertWithMessage:@"Ingresa el nombre completo"];
        return;
    }
    
    if(_txt_email.text.length == 0)
    {
        [self showAlertWithMessage:@"Ingresa un correo electrónico"];
        return;
    }
    else
    {
        if(![self validateEmailWithString:_txt_email.text])
        {
            [self showAlertWithMessage:@"Ingresa un correo electrónico válido"];
            return;
            
        }
    }
    if(_txt_phone.text.length == 0)
    {
        [self showAlertWithMessage:@"Ingresa un número telefónico"];
         return;
    }
    else
    {
         if(_txt_phone.text.length != 10)
        {
               [self showAlertWithMessage:@"Debes ingresar tu número móvil de 10 digitos"];
               return;
        }
    }
    
    NSString *message = [NSString stringWithFormat:@"¿Deseas generar un link de cobro para %@?", _txt_complete_name.text];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"FamilyDoc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];

    //Add Buttons

    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Aceptar"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                      [self generate];
                                }];

    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];

    //Add your buttons to alert controller
    [alert addAction:noButton];
    [alert addAction:yesButton];
    

    [self presentViewController:alert animated:YES completion:nil];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
