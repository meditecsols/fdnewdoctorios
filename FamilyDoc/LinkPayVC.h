//
//  LinkPayVC.h
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 23/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LinkPayControllerDelegate <NSObject>
- (void)didDismissViewController:(NSMutableDictionary*_Nonnull)dic withData:(NSMutableDictionary *_Nonnull)data;
@end

NS_ASSUME_NONNULL_BEGIN

@interface LinkPayVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtTotalCost;
@property (weak, nonatomic) IBOutlet UILabel *lblNegativeAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblComisionMeditec;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalLessComissions;
@property (nonatomic, weak) id<LinkPayControllerDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
