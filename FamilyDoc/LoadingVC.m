//
//  LoadingVC.m
//  
//
//  Created by Martin Gonzalez on 16/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "LoadingVC.h"

@interface LoadingVC ()

@end

@implementation LoadingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSArray *imageNames = @[@"spinner1", @"spinner2", @"spinner3",@"spinner4", @"spinner5", @"spinner6"];
    
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i < imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    
    // Normal Animation
    UIImageView *animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 50, 53, 43)];
    [animationImageView setCenter:CGPointMake(self.view.frame.size.width/2+6, self.view.frame.size.height/2)];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.200 green:0.188 blue:0.161 alpha:0.80]];
    animationImageView.animationImages = images;
    animationImageView.animationDuration = 0.8;
    
    [self.view addSubview:animationImageView];
    [animationImageView startAnimating];
}

@end
