//
//  LoginVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 16/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginVC : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPass;
@property (strong, nonatomic) IBOutlet UIButton *btnShowPass;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
- (IBAction)actionShowPass:(id)sender;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionForgetPass:(id)sender;
- (IBAction)actionBack:(id)sender;
@end
