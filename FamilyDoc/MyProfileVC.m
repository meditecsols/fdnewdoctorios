//
//  MyProfileVC.m
//  
//
//  Created by Martin Gonzalez on 16/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "MyProfileVC.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "CaptureSignVC.h"



@interface MyProfileVC (){
    NSMutableArray *arraySpeciality;
    UIPickerView *specialityPicker;
}

@end

@implementation MyProfileVC
@synthesize imgProfile;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tap];
    _txtSpecialty.delegate=self;
    _txtEmail.delegate=self;
    _txtCellPhone.delegate=self;
    _txtOfficePhone.delegate=self;
    
    
//    _btnService.transform = CGAffineTransformMakeScale(-1.0, 1.0);
//    _btnService.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    _btnService.imageView.contentMode=UIViewContentModeScaleAspectFit;
//    _btnService.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    _btnService.imageEdgeInsets = UIEdgeInsetsMake(15,15,15,15);
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
    _constraintHeightSpecialtyContainer.constant=50;
    [_btnAddSpecialty setHidden:YES];
    [_txtSpecialty setHidden:YES];
    [self.view layoutIfNeeded];
    _lblSpecialty.backgroundColor = [UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00];
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:self.lblSpecialty.bounds
                              byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(8,8)
                              ];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
    maskLayer.frame = _lblSpecialty.bounds;
    maskLayer.path = maskPath.CGPath;
    
    self.lblSpecialty.layer.mask = maskLayer;
    _lblSpecialtyList.text=@"";
    [self roundImageProfile];
    
    _lblNameDoctor.text=[NSString stringWithFormat:@"%@ %@ %@" ,[doctorInfo objectForKey:@"first_name"],[doctorInfo objectForKey:@"last_name"],[doctorInfo objectForKey:@"second_last_name"]];
    
    
    
    _lblGender.text=[doctorInfo objectForKey:@"gender"];
    if ([[doctorInfo objectForKey:@"gender"] isEqualToString:@"male"]) {
        _lblGender.text=@"Hombre";
        [_imgGender setImage:[UIImage imageNamed:@"iconMale"]];
        
    }
    else if ([[doctorInfo objectForKey:@"gender"] isEqualToString:@"female"]) {
        _lblGender.text=@"Mujer";
        [_imgGender setImage:[UIImage imageNamed:@"iconFemale"]];
        _lblDr.text=@"Dra.";
    }
    
    if ([[doctorInfo objectForKey:@"date_of_birth"] length]>0) {
        _lblBirthDate.text=[self formatDateToView:[doctorInfo objectForKey:@"date_of_birth"]];
    }
    if ([[doctorInfo objectForKey:@"speciality"] length]>0) {
        _lblSpecialtyList.text=[doctorInfo objectForKey:@"speciality"];
    }
    
    if ([[doctorInfo objectForKey:@"work_phone_number"] length]>0) {
        _txtOfficePhone.text=[doctorInfo objectForKey:@"work_phone_number"];
    }
    
    if ([[doctorInfo objectForKey:@"mobile_phone_number"] length]>0) {
        _txtCellPhone.text=[doctorInfo objectForKey:@"mobile_phone_number"];
    }
    if ([[doctorInfo objectForKey:@"username"] length]>0) {
        _txtEmail.text=[doctorInfo objectForKey:@"username"];
        [_txtEmail setEnabled:NO];
    }
    if ([[doctorInfo objectForKey:@"public_profile_preference"] boolValue]) {
        _btnCheckBox.selected=YES;
    }
    
    
    [self readProfileImageSaved];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(captureImage)];
    [imgProfile setUserInteractionEnabled:YES];
    [imgProfile addGestureRecognizer:tap];
    
    
    
    [self getSpecialityForDoctor];
    
    
}
-(void)getSpecialityForDoctor{
    InvokeService *invoke=[[InvokeService alloc] init];
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [invoke getSpecialtiesWithCompletion:^(NSMutableArray *responseData, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                NSLog(@"response:%@",[responseData description]);
                if ([responseData isKindOfClass:[NSDictionary class]]) {
                    [self showAlertWithMessage:@"Error al obtener las Especialdidades"];
                    return;
                }
                NSArray *data=[[NSArray alloc] initWithArray:responseData];
                
                NSString *spealityList=@"";
                if ([data count]>0) {
                    for (NSDictionary *dict in data) {
                        spealityList=[spealityList stringByAppendingString:[dict objectForKey:@"speciality_name"]];
                        spealityList=[spealityList stringByAppendingString:@", "];
                    }
                    if(spealityList.length >=2){
                        spealityList=[spealityList substringToIndex:spealityList.length-2];
                    }
                    
                    self->_lblSpecialtyList.text=spealityList;
                    
                }
                self->_constraintHeightSpecialtyContainer.constant=50;
                [self->_btnAddSpecialty setHidden:YES];
                [self->_txtSpecialty setHidden:YES];
                [self->_scrollContainer setContentOffset:CGPointZero animated:YES];
            });
            
        }];
    }];
}
-(NSString *)formatDateToView:(NSString *)inputDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *formattedDate = [dateFormatter dateFromString:inputDate]
    ;
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yyyy"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:formattedDate];
    return dateFinal;
}
-(void)readProfileImageSaved{
//    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    documentsDirectory=[documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/ProfileInfo/%@.jpg",[doctorInfo objectForKey:@"id"]]];
//    NSData *imgData = [NSData dataWithContentsOfFile:documentsDirectory];
//    UIImage *image = [[UIImage alloc] initWithData:imgData];
//
//    imgProfile.contentMode=UIViewContentModeScaleAspectFill;
//    NSFileManager *fileMgr = [NSFileManager defaultManager];
//    if ([fileMgr fileExistsAtPath:documentsDirectory]){
//        imgProfile.image = image;
//        [self roundImageProfile];
//    }
    if ([[doctorInfo objectForKey:@"profile_pic"] length]>0) {
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke getImageFromUrl:[doctorInfo objectForKey:@"profile_pic"] WithCompletion:^(UIImage * _Nullable imageDownloaded, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!error) {
                    [self->imgProfile setImage:imageDownloaded];
                    self->imgProfile.contentMode=UIViewContentModeScaleAspectFill;
                }
                
            });
            
        }];
    }
    
    
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width scaledToHeight:(float) i_height
{
    UIImage *newImage;
    if (sourceImage.size.width>i_width) {
        float oldWidth = sourceImage.size.width;
        float scaleFactor = i_width / oldWidth;
        
        float newHeight = sourceImage.size.height * scaleFactor;
        float newWidth = oldWidth * scaleFactor;
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else{
        newImage=sourceImage;
    }
    
    if(newImage.size.height>i_height){
        float oldHeight = sourceImage.size.height;
        float scaleFactor = i_height / oldHeight;
        
        float newWidth = sourceImage.size.width * scaleFactor;
        float newHeight =oldHeight * scaleFactor;
        
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return newImage;
}
-(void)roundImageProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 3.0f;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imgProfile.clipsToBounds = YES;
}
-(void)captureImage{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"Seleccione una opción" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [actionSheet dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Tomar foto" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = NO;
            [self presentViewController:imagePicker animated:YES completion:nil];
            //newMedia = YES;
        }
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Seleccionar existente" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = NO;
            imagePicker.modalPresentationStyle= UIModalPresentationOverFullScreen;
            [self presentViewController:imagePicker animated:YES completion:nil];
            //newMedia = NO;
        }
    }]];
    // Present action sheet.
    
    
    [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
    
    //For set action sheet to middle of view.
    CGRect rect = self.view.frame;
    rect.origin.x = self.view.frame.size.width / 20;
    rect.origin.y = self.view.frame.size.height / 20;
    actionSheet.popoverPresentationController.sourceView = self.view;
    actionSheet.popoverPresentationController.sourceRect = rect;
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
#pragma mark - Profile Image methods


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image =[self imageWithImage:[info objectForKey:UIImagePickerControllerOriginalImage] scaledToWidth:500 scaledToHeight:500] ;
    imgProfile.image = image;
    //[btnImagePicker setBackgroundImage:imageViewBack.image forState:UIControlStateNormal];
    //imageViewBack.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSData *imageData = UIImageJPEGRepresentation(image, 0);
    NSError *error;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory=[documentsDirectory stringByAppendingString:@"/ProfileInfo"];
    if (![fileMgr fileExistsAtPath:documentsDirectory]){
        [fileMgr createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    NSString *nameImage=[NSString stringWithFormat:@"%@.jpg",[doctorInfo objectForKey:@"id"]];
    NSString *path= [documentsDirectory stringByAppendingPathComponent:nameImage];
    
    [imageData writeToFile:path options:NSDataWritingAtomic error:&error];
    
    NSMutableDictionary *dictForUpdate=[[NSMutableDictionary alloc] init];

    [dictForUpdate setObject:path forKey:@"pathProfileImage"];

    if ([[dictForUpdate allKeys] count]>0) {
        InvokeService *invoke=[[InvokeService alloc] init];
        [[SharedFunctions sharedInstance] showLoadingView];
        [invoke updateDoctorWithData:dictForUpdate WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                if ([[responseData allKeys] count]>0) {
                    //[self showAlertWithMessage:@"Su imagen se guardó correctamente"];
                    [self readProfileImageSaved];
                }
                else{
                    [self showAlertWithMessage:@"Error al actualizar su imagen"];
                }
                
            });
            
        }];
    }
    
}

-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if(error){
        [self showAlertWithMessage:error.description];
        
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Picker Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arraySpeciality.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arraySpeciality[row] objectForKey:@"name"];
    
}
- (void) donePickingSpecialty {
    int row = (int) [specialityPicker selectedRowInComponent:0];
    _txtSpecialty.text =  [self pickerView:specialityPicker titleForRow:row forComponent:0];
    [self hideKeyboard];
}
-(void)addNewSpecialty{
    [_txtSpecialty resignFirstResponder];
    _txtSpecialty.inputView=nil;
    _txtSpecialty.text=@"";
    _txtSpecialty.inputAccessoryView = nil;
    [_txtSpecialty becomeFirstResponder];
}
- (void) hideKeyboard{
    [self.view endEditing:YES];
}

- (IBAction)saveNewSpecialty:(id)sender {
    [self.view endEditing:YES];
    if (_txtSpecialty.text>0) {
        if ([_lblSpecialtyList.text containsString:_txtSpecialty.text]) {
            [self showAlertWithMessage:@"Ya has agregado esa especialidad"];
            return;
        }
        [[SharedFunctions sharedInstance] showLoadingView];
        
        NSMutableDictionary *dictInfo=[[NSMutableDictionary alloc] init];
        int idSpeciality=-1;
        for (NSDictionary *dictSpeciality in arraySpeciality) {
            if ([[dictSpeciality objectForKey:@"name"] isEqualToString:_txtSpecialty.text]) {
                idSpeciality=[[dictSpeciality objectForKey:@"id"] intValue];
            }
        }
        if (idSpeciality<0) {
            InvokeService *invoke2=[[InvokeService alloc] init];
            NSMutableDictionary *dictSpecialty=[[NSMutableDictionary alloc] init];
            [dictSpecialty setObject:_txtSpecialty.text forKey:@"name"];
            [invoke2 addSpecialtyWithData:dictSpecialty WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                if ([[responseData allKeys] count] >0 && !error) {
                    [dictInfo setObject:[NSString stringWithFormat:@"%d",[[responseData objectForKey:@"id"] intValue]] forKey:@"speciality"];
                    [dictInfo setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
                    [dictInfo setObject:@"0" forKey:@"priority"];
                    
                    InvokeService *invoke=[[InvokeService alloc] init];
                    [invoke addSpecialtyToDoctorWithData:dictInfo WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[SharedFunctions sharedInstance] removeLoadingView];
                            [self getSpecialityForDoctor];
                        });
                        
                    }];
                }
            }];
        }
        [dictInfo setObject:[NSString stringWithFormat:@"%d",idSpeciality] forKey:@"speciality"];
        [dictInfo setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
        [dictInfo setObject:@"0" forKey:@"priority"];
        
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke addSpecialtyToDoctorWithData:dictInfo WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                [self getSpecialityForDoctor];
            });
            
        }];
    }
    else{
        [self showAlertWithMessage:@"Seleccione o ingrese una especialidad a agregar"];
    }
    
}
- (IBAction)actionOpendAddSpecialty:(id)sender {
    InvokeService *invoke=[[InvokeService alloc] init];
    [[SharedFunctions sharedInstance] showLoadingView];
    [invoke getSpecialtiesListWithCompletion:^(NSMutableArray *responseData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingView];
            NSLog(@"response:%@",[responseData description]);
            NSArray *data=[[NSArray alloc] initWithArray:responseData];
            //cambios martin no
            self->arraySpeciality=[[NSMutableArray alloc] initWithArray:data];
    
    
            if ([self->arraySpeciality count]>0) {
                self->specialityPicker = [[UIPickerView alloc]init];
                self->specialityPicker.tintColor = [UIColor whiteColor];
                self->specialityPicker.dataSource = self;
                self->specialityPicker.delegate = self;
                [self->_txtSpecialty setInputView:self->specialityPicker];
                UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
                toolBar.barStyle = UIBarStyleDefault;
                //UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Agregar Nuevo",nil) style:UIBarButtonItemStyleDone target:self action:@selector(addNewSpecialty)];
                UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
                UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingSpecialty)];
                toolBar.tintColor = [UIColor darkGrayColor];
                [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
                self->_txtSpecialty.inputAccessoryView = toolBar;
                //bloodField = cell.txtField;
                [self->_txtSpecialty becomeFirstResponder];
            }
            else{
                [self showAlertWithMessage:@"No se encontraron especialidades"];
            }
        });
        
    }];
    
    
    
    _constraintHeightSpecialtyContainer.constant=128;
    [_btnAddSpecialty setHidden:NO];
    [_txtSpecialty setHidden:NO];
    [self.view layoutIfNeeded];
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"\n\n\nPadecimiento"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    NSString *text =[NSString stringWithFormat:@"\n\n\nPadecimiento\n\n"];
    NSString *text2=message;
    NSString *text3=[NSString stringWithFormat:@"%@%@",text,text2];
    NSMutableAttributedString *attributedText =[[NSMutableAttributedString alloc] initWithString:text3];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:19] range:[text3 rangeOfString:text]];
    NSRange blackTextRange = [text3 rangeOfString:text2];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:15] range:blackTextRange];
    [alert setValue:attributedText forKey:@"attributedTitle"];
    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    
    
    
    [self presentViewController:alert animated:NO completion:^{
        int xPosition = alert.view.frame.size.width/2-20;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPosition,30,40,44)];
        imageView.image = [UIImage imageNamed:@"imgPadecimientoAlert"];
        [alert.view addSubview:imageView];
        
    }];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(BOOL)validatePhone : (NSString *)phoneNumber{
    NSString *phoneRegex = @"[0-9]{10}";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [test evaluateWithObject:phoneNumber];
}
- (IBAction)actionEdit:(id)sender {
    [self captureImage];
}
-(void)actionSave:(id)sender{
    [self.view endEditing:YES];
    if (_txtEmail.text.length>0&&![self validateEmailWithString:_txtEmail.text]) {
        [self showAlertWithMessage:@"Debes ingresar un Email válido"];
        return;
    }
    if (_txtOfficePhone.text.length>0 && ![self validatePhone:_txtOfficePhone.text]) {
        [self showAlertWithMessage:@"Debes ingresar el teléfono de oficina a 10 dígitos."];
        return;
    }
    if (_txtCellPhone.text.length>0 && ![self validatePhone:_txtCellPhone.text]) {
        [self showAlertWithMessage:@"Debes ingresar el teléfono celular a 10 dígitos."];
        return;
    }
    NSMutableDictionary *dictForUpdate=[[NSMutableDictionary alloc] init];
    if (![[doctorInfo objectForKey:@"email"] isEqualToString:_txtEmail.text]) {
        [dictForUpdate setObject:_txtEmail.text forKey:@"email"];
    }
    if (![[doctorInfo objectForKey:@"work_phone_number"] isEqualToString:_txtOfficePhone.text]) {
        [dictForUpdate setObject:_txtOfficePhone.text forKey:@"work_phone_number"];
    }
    if (![[doctorInfo objectForKey:@"mobile_phone_number"] isEqualToString:_txtCellPhone.text]) {
        [dictForUpdate setObject:_txtCellPhone.text forKey:@"mobile_phone_number"];
    }
    if ([_btnCheckBox isSelected]) {
        [dictForUpdate setObject:@YES forKey:@"public_profile_preference"];
    }
    else{
        [dictForUpdate setObject:@NO forKey:@"public_profile_preference"];
    }
    if ([[dictForUpdate allKeys] count]>0) {
        InvokeService *invoke=[[InvokeService alloc] init];
        [[SharedFunctions sharedInstance] showLoadingView];
        [invoke updateDoctorWithData:dictForUpdate WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                if ([[responseData allKeys] count]>0) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else{
                    [self showAlertWithMessage:@"Error al actualizar sus datos"];
                }
                
            });
            
        }];
    }
}
- (IBAction)actionBack:(id)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
  
}
- (IBAction)actionAddress:(id)sender {
}

- (IBAction)actionSign:(id)sender {
        UIStoryboard *storyboard = self.storyboard;
        CaptureSignVC *capture= [storyboard instantiateViewControllerWithIdentifier:@"CaptureSignVC"];
        capture.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    capture.modalPresentationStyle= UIModalPresentationOverFullScreen;
    capture.comeFromMyProfile=YES;
        [self presentViewController:capture animated:YES completion:^{

        }];

}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [_scrollContainer setContentOffset:CGPointMake(0, (textField.superview.frame.origin.y + (textField.frame.origin.y)-300)) animated:YES];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL cambiar= YES;
    if (textField==_txtCellPhone ||textField==_txtOfficePhone) {
        
        if (textField.text.length > 9 && range.length == 0)
        {
            cambiar=NO;
        }
        else
        {
            
            NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789 "] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
            cambiar= [string isEqualToString:filtered];
            if (textField.text.length==9&&![string isEqualToString:@""]&&cambiar) {
                textField.text=[NSString stringWithFormat:@"%@%@",textField.text,string];
                [textField resignFirstResponder];
                
            }
            
            
        }
    }
    return cambiar;
}
- (IBAction)actionCheckBox:(id)sender {
    if ([self.btnCheckBox isSelected]) {
        self.btnCheckBox.selected = NO;
    }
    else{
        self.btnCheckBox.selected = YES;
        
    }

}

@end
