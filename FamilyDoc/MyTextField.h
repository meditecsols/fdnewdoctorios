//
//  UITextField+MyTextField.h
//  Claro 360
//
//  Created by Martin Gonzalez Escamilla on 10/02/17.
//  Copyright © 2017 Selene Andraca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTextField : UITextField
@property (nonatomic) IBInspectable CGFloat padding;
@end
