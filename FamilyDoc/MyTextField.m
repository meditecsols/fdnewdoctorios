//
//  UITextField+MyTextField.m
//  Claro 360
//
//  Created by Martin Gonzalez Escamilla on 10/02/17.
//  Copyright © 2017 Selene Andraca. All rights reserved.
//

#import "MyTextField.h"

@implementation MyTextField
@synthesize padding;

-(CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, padding, 0);
}

-(CGRect)editingRectForBounds:(CGRect)bounds{
    return [self textRectForBounds:bounds];
}
@end
