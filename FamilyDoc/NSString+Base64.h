//
//  NSString+Base64.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 7/31/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Base64)
+ (NSString *) base64StringFromData: (NSData *)data;
+ (NSString *) base64StringFromData: (NSData *)data length: (int)length;
@end

NS_ASSUME_NONNULL_END
