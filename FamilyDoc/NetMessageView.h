

#import <UIKit/UIKit.h>

@interface NetMessageView : UIView
{
    NSString *message;
    NSNumber *wtag;
    UIColor *color;
    
}


@property (strong,nonatomic) NSString *message;
@property (strong,nonatomic) NSNumber *wtag;
@property (strong,nonatomic) UIColor *color;



- (id)initWithFrame:(CGRect)frame Message:(NSString*)message Tag:(NSNumber *)tag Color:(UIColor *)backgroundColor;
-(void)initializeController;

@end
