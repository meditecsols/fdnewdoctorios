

#import "NetMessageView.h"

@implementation NetMessageView
@synthesize message,wtag,color;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
  
    }
    return self;
}




- (id)initWithFrame:(CGRect)frame Message:(NSString*)lmessage Tag:(NSNumber *)tag Color:(UIColor *)backgroundColor
{

    self = [super initWithFrame:frame];
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;

    [self setMessage:lmessage];
    [self setWtag:tag];
    [self setBackgroundColor:backgroundColor];
    
    if (self = [super init]) {
        
        [self initializeController];
        
    }
    return self;
}

-(void)initializeController{
    
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    
    if (self.backgroundColor == nil ||self.backgroundColor == NULL)
    {
        [self setBackgroundColor:[UIColor clearColor]];
    }else{
        [self setBackgroundColor:self.backgroundColor];
    }
    
    UILabel *lblTitle = [[UILabel alloc] init];
    
    if (message != nil) {
        lblTitle.text = message;
        [lblTitle setTextColor:[UIColor whiteColor]];
        [lblTitle setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        NSLog(@"self.frame.size.width = %f",self.frame.size.width);
        lblTitle.textAlignment = NSTextAlignmentCenter;
        //[lblTitle setBackgroundColor:[UIColor blueColor]];
        [lblTitle setFrame:CGRectMake(5,70, self.frame.size.width-10, 20)];
        lblTitle.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        //[lblTitle setBackgroundColor:[UIColor greenColor]];
        
        [self addSubview:lblTitle];
        
    }
    
    if (wtag != nil) {
        self.tag = 5;
    }else{
        [self setWtag:wtag];
    }
    
}

@end
