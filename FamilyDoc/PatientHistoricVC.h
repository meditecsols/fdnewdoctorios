//
//  PatientHistoricVC.h
//  
//
//  Created by Martin Gonzalez on 01/03/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatientHistoricVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
@property NSMutableArray *consultasArray;

@property (strong, nonatomic) IBOutlet UITableView *patientsTV;
@property (strong, nonatomic) IBOutlet UIWebView *myWebView;
- (IBAction)closeWebView:(id)sender;
- (IBAction)actionBack:(id)sender;

@end
