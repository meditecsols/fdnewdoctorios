//
//  PatientTVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 28/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatientTVC : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UIImageView *imgPatient;
@property (strong, nonatomic) IBOutlet UIImageView *imgNew;
@property (strong, nonatomic) IBOutlet UILabel *lblNewPatiente;
@property (strong, nonatomic) IBOutlet UILabel *lblNumberMessages;
@property (strong, nonatomic) IBOutlet UILabel *lblPendingPayment;
@property (strong, nonatomic) IBOutlet UILabel *lblCompany;
@end
