/*
 * QRCodeReaderViewController
 *
 * Copyright 2014-present Yannick Loriot.
 * http://yannickloriot.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#import "QRCodeReaderView.h"

@interface QRCodeReaderView ()
@property (nonatomic, strong) CAShapeLayer *overlay;

@end

@implementation QRCodeReaderView

- (id)initWithFrame:(CGRect)frame
{
  if ((self = [super initWithFrame:frame])) {
    [self addOverlay];
  }

  return self;
}

- (void)drawRect:(CGRect)rect
{
    
    
    
    UIImageView *imageFrame=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-10, 200)];
    imageFrame.center=self.center;
    [imageFrame setImage:[UIImage imageNamed:@"qrFrame"]];
    [self addSubview:imageFrame];
    
    UIView *bg1=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 180)];
    [bg1 setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:bg1];
    
    UIImageView *imgQr=[[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width/2)-25,50, 50, 50)];
    [imgQr setImage:[UIImage imageNamed:@"qrScanImage"]];
    [bg1 addSubview:imgQr];
    
    UILabel *text=[[UILabel alloc] initWithFrame:CGRectMake(10, 90, self.frame.size.width-20, 80)];
    NSString *textPlain=@"Escanea el código QR de tu paciente\n para registrar la consulta.";
    NSString *text2=@"código QR";
    NSMutableAttributedString *attributedText =[[NSMutableAttributedString alloc] initWithString:textPlain attributes:@{
                                                                                                                        NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Regular" size:15],
                                                                                                                        NSForegroundColorAttributeName:[UIColor grayColor]
                                                                                                                        }];
    NSRange blueTextRange = [textPlain rangeOfString:text2];
    [attributedText addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00] range:blueTextRange];
    [text setAttributedText:attributedText];
    
    [text setTextAlignment:NSTextAlignmentCenter];
    text.numberOfLines=4;
    [self addSubview:text];
    
    UIView *bg2=[[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-150, self.frame.size.width, 150)];
    [bg2 setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:bg2];
    
    UILabel *textLabel2=[[UILabel alloc] initWithFrame:CGRectMake(10, bg2.frame.origin.y+5, self.frame.size.width-20, 130)];
    NSString *stringMessage=@"¿EN DÓNDE ESTÁ EL CÓDIGO?\nPide a tu paciente que te muestre el código QR desde su app.\n\n Está en la esquina superior derecha.";
    
    NSString *text3=@"¿EN DÓNDE ESTÁ EL CÓDIGO?";
    NSString *text4=@"Está en la esquina superior derecha.";
    NSMutableAttributedString *attributedText2 =[[NSMutableAttributedString alloc] initWithString:stringMessage attributes:@{
                                                                                                                        NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Regular" size:15],
                                                                                                                        NSForegroundColorAttributeName:[UIColor grayColor]
                                                                                                                        }];
    NSRange blackTextRange = [stringMessage rangeOfString:text3];
    NSRange italicTextRange = [stringMessage rangeOfString:text4];
    [attributedText2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Bold" size:15] range:blackTextRange];
    [attributedText2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Light" size:15] range:italicTextRange];

    
    [textLabel2 setAttributedText:attributedText2];
    [textLabel2 setTextAlignment:NSTextAlignmentCenter];
    textLabel2.numberOfLines=6;
    [self addSubview:textLabel2];
}

#pragma mark - Private Methods

- (void)addOverlay
{
  _overlay = [[CAShapeLayer alloc] init];
  _overlay.backgroundColor = [UIColor clearColor].CGColor;
  _overlay.fillColor       = [UIColor clearColor].CGColor;
  _overlay.strokeColor     = [UIColor blueColor].CGColor;
  _overlay.lineWidth       = 3;
  _overlay.lineDashPattern = @[@7.0, @7.0];
  _overlay.lineDashPhase   = 0;

  [self.layer addSublayer:_overlay];
}

@end
