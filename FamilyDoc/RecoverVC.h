//
//  RecoverVC.h
//  
//
//  Created by Martin Gonzalez on 19/01/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecoverVC : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionBack:(id)sender;

@end
