//
//  Register2VC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 07/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Register2VC : UIViewController<UIImagePickerControllerDelegate,UITextFieldDelegate,UINavigationControllerDelegate>
@property BOOL comeFromRegister;
@property NSString *fullName;
@property NSString *email;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutTopView;
@property (strong, nonatomic) IBOutlet UIView *divViewCaptureAccount;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCaptureAccount;
@property (strong, nonatomic) IBOutlet UILabel *lblViewCaptureAccount;
@property (strong, nonatomic) IBOutlet UIView *divViewSignature;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewSignature;
@property (strong, nonatomic) IBOutlet UILabel *lblViewSignature;
@property (strong, nonatomic) IBOutlet UITextField *txtYear;
@property (strong, nonatomic) IBOutlet UITextField *txtMonth;
@property (strong, nonatomic) IBOutlet UITextField *txtDay;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;
@property (strong, nonatomic) IBOutlet UITextField *txtClabe;
@property (strong, nonatomic) IBOutlet UIView *viewAccount;
@property (strong, nonatomic) IBOutlet UIView *viewSignature;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollContent;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckBox;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionDismissPopUp:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblBankStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblSignStatus;
- (IBAction)actionCaptureBankAccount:(id)sender;
- (IBAction)actionSigning:(id)sender;
- (IBAction)actionCheckBox:(id)sender;

@end
