//
//  Register3VC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 07/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Register3VC : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtStreet;
@property (strong, nonatomic) IBOutlet UITextField *txtZipCode;
@property (strong, nonatomic) IBOutlet UITextField *txtNeighbordHood;
@property (strong, nonatomic) IBOutlet UITextField *txtProvince;
@property (strong, nonatomic) IBOutlet UITextField *txtState;
@property (strong, nonatomic) IBOutlet UITextField *txtExtNumb;
@property (strong, nonatomic) IBOutlet UITextField *txtInt;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UITextField *txtCountry;
@property (strong, nonatomic) IBOutlet UIButton *btnSkip;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionSkip:(id)sender;

@end
