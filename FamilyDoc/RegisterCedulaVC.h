//
//  RegisterCedulaVC.h
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 22/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegisterCedulaVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scroll_content;
@property (weak, nonatomic) IBOutlet UITextField *txt_cedula;
@property (weak, nonatomic) IBOutlet UITextField *txt_number;

@end

NS_ASSUME_NONNULL_END
