//
//  RegisterCedulaVC.m
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 22/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import "RegisterCedulaVC.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "RegisterVC.h"

@interface RegisterCedulaVC ()
{
    NSDictionary *data;
    int movedHeight;
    UITextField *textFieldEditable;
}

@end

@implementation RegisterCedulaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self
                                      action:@selector(dismissKeyboard)];
       
       [self.view addGestureRecognizer:tap];

       
       [[NSNotificationCenter defaultCenter] addObserver:self
                                                selector:@selector(keyboardWillShow:)
                                                    name:UIKeyboardWillShowNotification object:nil];
       [[NSNotificationCenter defaultCenter] addObserver:self
                                                selector:@selector(keyboardWillHide:)
                                                    name:UIKeyboardWillHideNotification object:nil];
    
    _txt_cedula.delegate = self;
    _txt_number.delegate = self;
    
    // Do any additional setup after loading the view.
}
- (IBAction)action_siguiente:(id)sender {
    
    if(_txt_cedula.text.length == 0)
    {
        [self showAlertWithMessage:@"Debes ingresar tu Cédula"];
        return;
    }
    
    if(_txt_number.text.length == 0)
    {
        [self showAlertWithMessage:@"Debes ingresar tu número móvil"];
        return;
    }
    
    if(_txt_number.text.length != 10)
    {
        [self showAlertWithMessage:@"Debes ingresar tu número móvil de 10 digitos"];
        return;
    }

    //[self dismissViewControllerAnimated:true completion:nil];

    [self performSegueWithIdentifier:@"segue_register" sender:nil];
//    if (_txt_cedula.text.length<10) {
//
//        [_txt_cedula resignFirstResponder];
//        [[SharedFunctions sharedInstance] showLoadingView];
//
//        InvokeService *invoke=[[InvokeService alloc] init];
//
//        [invoke retrieveCedulaInfo:_txt_cedula.text withPhone:_txt_number.text WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                NSLog(@"response:%@",[responseData description]);
//                [[SharedFunctions sharedInstance] removeLoadingView];
//                if ([responseData isKindOfClass:[NSDictionary class]]) {
//                    if ([[responseData objectForKey:@"detail"] isEqualToString:@"Not found."]||[[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]||[[responseData allKeys] count]<=0) {
//                        [[SharedFunctions sharedInstance] removeLoadingView];
//                        [self showAlertWithMessage:@"Intenta de nuevo"];
//
//                        //self->_txt_cedula.text=@"";
//
//                        return;
//                    }
//                }
//                else{
//                    self->data=[[NSDictionary alloc] initWithDictionary:[(NSArray *)responseData objectAtIndex:0]];
//                    NSLog(@"data:%@",[self->data description]);
//                    [self dismissViewControllerAnimated:true completion:nil];
//                    [self performSegueWithIdentifier:@"segue_register" sender:nil];
//
//                    [[SharedFunctions sharedInstance] removeLoadingView];
//                }
//
//            });
//
//        }];
//
//    }
//    else{
//        [self showAlertWithMessage:@"Ingrese una cédula válida."];
//    }
    
    
    
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.scroll_content setScrollEnabled:YES];
    textFieldEditable=textField;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.txt_number)
    {
        if (textField.text.length < 10 || string.length == 0)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return YES;
}

-(void)keyboardWillShow:(NSNotification*)notification{
    NSDictionary* info = [notification userInfo];
    
    CGRect kKeyBoardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    int KeyboardY=kKeyBoardFrame.origin.y;
    int endPositionText=textFieldEditable.frame.origin.y+textFieldEditable.frame.size.height+20;
    //if (endPositionText<KeyboardY) {
        //textFieldEditable=nil;
       // return;
   // }
    int animatedDistance =80+endPositionText-KeyboardY;
    const int movementDistance = animatedDistance;
    int movement = -movementDistance;
    movedHeight=-movement;
    [self.scroll_content setContentOffset:CGPointMake(0, 80) animated:YES];
   
    
}
-(void)keyboardWillHide:(NSNotification*)notification{
//    if (textFieldEditable==nil) {
//        return;
//    }
    [self.scroll_content setContentOffset:CGPointMake(0,0) animated:YES];
    
}
- (IBAction)back_action:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Aceptar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"segue_register"])
    {
        RegisterVC *vc = [segue destinationViewController];
        vc.data = self->data;
        vc.cedula = _txt_cedula.text;
        vc.phone = _txt_number.text;
        
        
    }
}

@end
