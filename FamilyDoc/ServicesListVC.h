//
//  ServicesListVC.h
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 27/07/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServicesListVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollServices;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UITableView *tableServices;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPrice;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTable;
@property (strong, nonatomic) IBOutlet UIView *viewAddServiceContainer;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightViewContainer;
@property NSMutableDictionary *dictService;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionAddService:(id)sender;
- (IBAction)actionCancelAddService:(id)sender;
- (IBAction)actionRegisterService:(id)sender;

@end
