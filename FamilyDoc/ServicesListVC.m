//
//  ServicesListVC.m
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 27/07/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "ServicesListVC.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "RMCustomViewActionController.h"
#import "AddServiceVC.h"

#define HEIGHT_ROW 40

@interface ServicesListVC (){
    NSMutableArray *arrayServices;
}

@end

@implementation ServicesListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrayServices=[[NSMutableArray alloc] init];
    _tableServices.delegate=self;
    _tableServices.dataSource=self;
    _layoutConstraintHeightTable.constant=0;
    [_viewAddServiceContainer setHidden:YES];
    
  //  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
   //                                initWithTarget:self
   //                                action:@selector(dismissKeyboard)];
    
   // [self.view addGestureRecognizer:tap];
    _txtFieldPrice.delegate=self;
    _txtFieldName.delegate=self;
    self.dictService=[[NSMutableDictionary alloc] init];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getServices];
}
-(void)dismissKeyboard {
    [_txtFieldPrice resignFirstResponder];
    [_txtFieldName resignFirstResponder];
}
-(void)getServices{
    InvokeService *invoke=[[InvokeService alloc] init];
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [invoke getServicesWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance]removeLoadingView];  //WithCompletition:^{
                if (!error) {
                    if ([responseData isKindOfClass:[NSArray class]]) {
                        if ([responseData count]>0) {
                            self->arrayServices=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:responseData] mutableCopy];
                            
                        }
                        else if([responseData count]==0){
                            self->arrayServices=[[NSMutableArray alloc] init];
                        }
                        [self->_tableServices reloadData];
                        [self resizeTable];
                    }
                }
                //}];
                
            });
            
        }];
    }];
    
}
-(void)resizeTable{
    
    _layoutConstraintHeightTable.constant=HEIGHT_ROW *[arrayServices count];
    _layoutConstraintHeightViewContainer.constant= _layoutConstraintHeightTable.constant + 600;
    _tableServices.scrollEnabled=NO;
    
    
    [self.view layoutSubviews];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayServices count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HEIGHT_ROW;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[self.tableServices dequeueReusableCellWithIdentifier:@"serviceCell"];
    UILabel *lblNameService = (UILabel *)[cell viewWithTag:1];
    UILabel *lblPrice= (UILabel *)[cell viewWithTag:2];
    lblNameService.text=[[arrayServices objectAtIndex:indexPath.row] objectForKey:@"name"];
    lblPrice.text=[NSString stringWithFormat:@"$%@",[[arrayServices objectAtIndex:indexPath.row] objectForKey:@"price"]];
    
    UIButton *btnDelete=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [btnDelete setImage:[UIImage imageNamed:@"lessIcon"] forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(removeService:) forControlEvents:UIControlEventTouchUpInside];
    btnDelete.tag = indexPath.row;
    [cell addSubview:btnDelete];
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    RMActionControllerStyle style = RMActionControllerStyleWhite;
    RMCustomViewActionController *actionController = [RMCustomViewActionController actionControllerWithStyle:style];
    
    
    RMAction *editAction = [RMAction<UIView *> actionWithTitle:@"Editar Servicio"  style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        
        [actionController dismissViewControllerAnimated:YES completion:^{
            self.dictService=[[NSMutableDictionary alloc] init];
            self.dictService=[[self->arrayServices objectAtIndex:indexPath.row] mutableCopy];
            [self performSelector:@selector(actionAddService:) withObject:nil afterDelay:0];
            
        }];
    }];
    
    RMAction *cancelAction = [RMAction<UIView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIView *> *controller) {
        NSLog(@"Action controller was canceled");
    }];
    
    
    actionController.title = @"";
    actionController.message = @"";
    
    [actionController addAction:cancelAction];
   // [actionController addAction:deleteAction];
    [actionController addAction:editAction];
    
    [self presentViewController:actionController animated:YES completion:nil];
    
}
- (void)removeService:(id)sender{
    UIButton *btnSender=(UIButton *)sender;
    int index=(int)btnSender.tag;
    InvokeService *invoke=[[InvokeService alloc] init];
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [invoke deleteService:[[self->arrayServices objectAtIndex:index]objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                    if (!error) {
                        if ([responseData isKindOfClass:[NSDictionary class]]) {
                            if ([[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]) {
                                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al eliminar tu servicio"];
                            }
                            else{
                                [self getServices];
                            }
                            
                        }
                        else{
                            [self getServices];
                        }
                        
                    }
                    else{
                        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al eliminar tu tarjeta"];
                    }
                }];
                
                
                
            });
        }];
            
    }];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionAddService:(id)sender {
//    UIStoryboard *storyboard = self.storyboard;
//    AddServiceVC  *vc= [storyboard instantiateViewControllerWithIdentifier:@"AddServiceVC"];
//
//    [self presentViewController:vc animated:YES completion:nil];
    
    [_viewAddServiceContainer setHidden:NO];
    if ([[_dictService allKeys] count]>0) {
        _txtFieldName.text=[_dictService objectForKey:@"name"];
        _txtFieldPrice.text=[NSString stringWithFormat:@"$%@",[_dictService objectForKey:@"price"]];
    }
}

- (IBAction)actionCancelAddService:(id)sender {
    [self hideAddContainer];
}

- (IBAction)actionRegisterService:(id)sender {
    [self.view endEditing:YES];
    if ([_txtFieldName.text length]<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar el nombre del servicio"];
        return;
    }
    if ([_txtFieldName.text isEqualToString:@"$0.00"]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar el precio del servicio"];
        return;
    }
    if ([[_dictService allKeys] count]>0) {
        NSMutableDictionary *dictServiceLocal=[[NSMutableDictionary alloc] init];
        [dictServiceLocal setObject:_txtFieldName.text forKey:@"name"];
        NSString *priceService=[_txtFieldPrice.text substringFromIndex:1];
        priceService=[priceService stringByReplacingOccurrencesOfString:@"," withString:@""];
        [dictServiceLocal setObject:priceService forKey:@"price"];
        [dictServiceLocal setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
        [dictServiceLocal setObject:[_dictService objectForKey:@"id"] forKey:@"id"];
        
        
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke updateServiceWithData:dictServiceLocal WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        
                        if (!error) {
                            if ([[responseData allKeys]count]>0) {
                                UIAlertController * alert=  [UIAlertController
                                                             alertControllerWithTitle:@""
                                                             message:@"Se ha actualizado el servicio exitosamente"
                                                             preferredStyle:UIAlertControllerStyleAlert];
                                
                                
                                UIAlertAction* okAction = [UIAlertAction
                                                           actionWithTitle:@"Entendido"
                                                           style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                                           {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                               [self getServices];
                                                               [self hideAddContainer];
                                                               
                                                           }];
                                
                                [alert addAction:okAction];
                                [self presentViewController:alert animated:NO completion:nil];
                                
                                
                            }
                            else{
                                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al actualizar el servicio."];
                            }
                        }
                        else{
                            [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al actualizar el servicio."];
                        }
                    }];
                    
                });
            }];
        }];
    }
    else{
        NSMutableDictionary *dictServiceLocal=[[NSMutableDictionary alloc] init];
        [dictServiceLocal setObject:_txtFieldName.text forKey:@"name"];
        NSString *priceService=[_txtFieldPrice.text substringFromIndex:1];
        priceService=[priceService stringByReplacingOccurrencesOfString:@"," withString:@""];
        [dictServiceLocal setObject:priceService forKey:@"price"];
        [dictServiceLocal setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke addServiceWithData:dictServiceLocal WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        if (!error) {
                            if ([[responseData allKeys]count]>0) {
                                UIAlertController * alert=  [UIAlertController
                                                             alertControllerWithTitle:@""
                                                             message:@"Se ha guardado el servicio exitosamente"
                                                             preferredStyle:UIAlertControllerStyleAlert];
                                
                                
                                UIAlertAction* okAction = [UIAlertAction
                                                           actionWithTitle:@"Entendido"
                                                           style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                                           {
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                               [self getServices];
                                                               [self hideAddContainer];
                                                               
                                                           }];
                                
                                [alert addAction:okAction];
                                [self presentViewController:alert animated:NO completion:nil];
                                
                                
                            }
                            else{
                                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al registrar el servicio."];
                            }
                        }
                        else{
                            [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al registrar el servicio."];
                        }
                    }];
                    
                });
            }];
        }];
    }
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:_txtFieldPrice]) {
        NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSInteger centValue = [cleanCentString intValue];
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        NSNumber *myNumber = [f numberFromString:cleanCentString];
        NSNumber *result;
        
        if([textField.text length] < 16){
            if (string.length > 0)
            {
                centValue = centValue * 10 + [string intValue];
                double intermediate = [myNumber doubleValue] * 10 +  [[f numberFromString:string] doubleValue];
                result = [[NSNumber alloc] initWithDouble:intermediate];
            }
            else
            {
                centValue = centValue / 10;
                double intermediate = [myNumber doubleValue]/10;
                result = [[NSNumber alloc] initWithDouble:intermediate];
            }
            
            myNumber = result;
            NSLog(@"%ld ++++ %@", (long)centValue, myNumber);
            NSNumber *formatedValue;
            formatedValue = [[NSNumber alloc] initWithDouble:[myNumber doubleValue]/ 100.0f];
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            textField.text = [_currencyFormatter stringFromNumber:formatedValue];
            
            
            return NO;
        }else{
            
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            textField.text = [_currencyFormatter stringFromNumber:@0];
            
            return NO;
        }
    }
    else{
        return YES;
    }
    
    
    return YES;
}
-(void)hideAddContainer{
    [_viewAddServiceContainer setHidden:YES];
    _txtFieldName.text=@"";
    _txtFieldPrice.text=@"";
    self.dictService=[[NSMutableDictionary alloc] init];
}
@end
