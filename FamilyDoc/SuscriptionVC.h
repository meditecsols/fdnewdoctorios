//
//  SuscriptionVC.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 8/7/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
//in-app purchase
#import <StoreKit/StoreKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SuscriptionVC : UIViewController<SKProductsRequestDelegate,SKPaymentTransactionObserver>
@property BOOL comeFromScan;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) SKProductsRequest *productsRequest;
@property (strong, nonatomic) SKPaymentQueue *defaultQueue;


- (IBAction)actionSuscribe:(id)sender;
- (IBAction)actionLater:(id)sender;
- (IBAction)actionRestore:(id)sender;
- (IBAction)actionOpenChat:(id)sender;


@end

NS_ASSUME_NONNULL_END
