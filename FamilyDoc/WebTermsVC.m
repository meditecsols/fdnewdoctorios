//
//  WebVideoVC.m
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 05/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "WebTermsVC.h"


@interface WebTermsVC (){
   // WKWebView *webView;
}

@end

@implementation WebTermsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnTerms.layer.cornerRadius=10;
    _btnTerms.layer.borderColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00].CGColor;
    _btnTerms.layer.borderWidth=1;
    
    _btnPrivacity.layer.cornerRadius=10;
    _btnPrivacity.layer.borderColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00].CGColor;
    _btnPrivacity.layer.borderWidth=1;
    
    
    
}
-(void)viewWillLayoutSubviews{
    
}
-(void)loadUrlWithUrlString:(NSString *)url andTitle:(NSString *)title{
    _lblTitle.text=title;
    _webView = [[WKWebView alloc] init];
    [_webView setFrame:CGRectMake(0, 0, _webContainerView.frame.size.width, _webContainerView.frame.size.height)];
    _webView.navigationDelegate = self;
    
    NSURL *nsurl=[NSURL URLWithString:url];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [_webView loadRequest:nsrequest];
    [self.webContainerView addSubview:_webView];
}
- (IBAction)closeView:(id)sender {
    if (![_btnPrivacity isHidden]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [_btnPrivacity setHidden:NO];
        [_btnTerms setHidden:NO];
        [_webView removeFromSuperview];
        _lblTitle.text=@"Acerca de";
    }
    
}

- (IBAction)actionOpenTerms:(id)sender {
    for (NSDictionary *dict in _arrayUrls) {
        if ([[dict objectForKey:@"label"] isEqualToString:@"terminos"]) {
            NSString *url=[dict objectForKey:@"url"];
            if ([url length]>0) {
                [self loadUrlWithUrlString:url andTitle:@"Términos y Condiciones"];
                [_btnPrivacity setHidden:YES];
                [_btnTerms setHidden:YES];
            }
        }
    }
    
    
    
}

- (IBAction)actionOpenPrivacity:(id)sender {
    for (NSDictionary *dict in _arrayUrls) {
        if ([[dict objectForKey:@"label"] isEqualToString:@"segunda"]) {
            NSString *url=[dict objectForKey:@"url"];
            if ([url length]>0) {
                [self loadUrlWithUrlString:url andTitle:@"Aviso de Privacidad"];
                [_btnPrivacity setHidden:YES];
                [_btnTerms setHidden:YES];
            }
        }
    }
}
@end
