//
//  WebVideoVC.h
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 05/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WKYTPlayerView.h"

@interface WebVideoVC : UIViewController
@property (strong, nonatomic) IBOutlet WKYTPlayerView *playerView;
- (IBAction)closeView:(id)sender;

@end
