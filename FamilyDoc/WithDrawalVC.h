//
//  WithDrawalVC.h
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 12/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithDrawalVC : UIViewController <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tv_types;
@property (weak, nonatomic) IBOutlet UITextField *lbl_amount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_total_withdrawal;

@end

NS_ASSUME_NONNULL_END
