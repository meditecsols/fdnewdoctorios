//
//  WithDrawalVC.m
//  FamilyDocDoctor
//
//  Created by Arturo Escamilla on 12/04/20.
//  Copyright © 2020 Martin Gonzalez. All rights reserved.
//

#import "WithDrawalVC.h"
#import "InvokeService.h"
#import "WithdrawalTypeTVC.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"



@interface WithDrawalVC ()

@end

@implementation WithDrawalVC
{
    NSMutableArray *types_array;
    NSUInteger selected_row;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
       
       UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
       UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
       UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44.f)];
       [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
       self.lbl_amount.inputAccessoryView = toolbar;
    
    
    self.lbl_amount.delegate = self;
    self.tv_types.backgroundColor = [UIColor whiteColor];
    types_array = [NSMutableArray new];
    InvokeService *invoke = [InvokeService new];
    
    
    [invoke getWithdrawalTypesData:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self->types_array = responseData;
            [self.tv_types reloadData];
        });
            
        
    }];

    // Do any additional setup after loading the view.
}
- (IBAction)action_withdrawal:(id)sender {
    

    double amount = [self convertCurrencyToDouble:_lbl_amount.text];
    
    if(amount < 50)
    {
        [self showAlertWithMessage:@"El monto minimo a retirar es de $50 pesos"];
        return;
    }
        
    InvokeService *inv = [InvokeService new];
    
    NSDictionary *dic_type = [types_array objectAtIndex:selected_row];
    
    int id_type = [[dic_type objectForKey:@"id"] intValue];

    
    
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic setValue:[NSNumber numberWithDouble:amount] forKey:@"amount"];
    [dic setValue:[NSNumber numberWithInt:id_type] forKey:@"withdrawal_type"];
    [dic setValue:[doctorInfo objectForKey:@"id"] forKey:@"user"];
    
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [inv addWithdrawalWithData:dic WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        
         if ([[responseData allKeys]count]>0) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"WithdrawalDismissed"
                     object:nil userInfo:nil];
                     [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
                 });
             
         }
        
        
        }];
    }];
    
    
}

-(double) convertCurrencyToDouble:(NSString *)input {
     
     NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    numberFormatter.locale = [NSLocale currentLocale];
    
    return [[numberFormatter numberFromString:input] doubleValue];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *simpleTab = @"WithdrawalTypeTVC";
    WithdrawalTypeTVC *cell = (WithdrawalTypeTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTab];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTab owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.backgroundColor = [UIColor whiteColor];
    
    NSDictionary *dic = [types_array objectAtIndex:indexPath.row];
    cell.lbl_name.text = [dic objectForKey:@"name"];
    cell.lbl_percenetage.text = [NSString stringWithFormat:@"Comisión -%@ %%",[dic objectForKey:@"percenetage"]];
    if(selected_row == indexPath.row)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [types_array count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     
    //WithdrawalTypeTVC *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    selected_row = indexPath.row;
    [self calculatepercentage:_lbl_amount.text];
   
    
    
    [self.tv_types reloadData];
    
    
}
-(void)calculatepercentage:(NSString *)amount_text
{
    NSDictionary *types = [types_array objectAtIndex:selected_row];
    double percenetage = [[types objectForKey:@"percenetage"] doubleValue];
    
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    double amount =  [[nf numberFromString:amount_text] doubleValue];
    
    
    double result_percentage = percenetage * amount / 100;
    
    double final_result = amount - result_percentage;
    final_result = final_result * 100;
    NSNumber *formatedValue;
    formatedValue = [[NSNumber alloc] initWithDouble:final_result / 100.0f];
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    _lbl_total_withdrawal.text = [_currencyFormatter stringFromNumber:formatedValue];
    
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    NSInteger centValue = [cleanCentString intValue];
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    NSNumber *myNumber = [f numberFromString:cleanCentString];
    NSNumber *result;
    
//double balance = [[doctorAccountInfo objectForKey:@"balance"] doubleValue];
    
    NSString *cleanBalString = [[[doctorAccountInfo objectForKey:@"balance"] componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    //NSInteger centBalValue = [cleanBalString intValue];
    NSNumberFormatter * fb = [[NSNumberFormatter alloc] init];
    NSNumber *myNumberBal = [fb numberFromString:cleanBalString];
    
    

    if(myNumberBal > 0)
    {
    if([textField.text length] < 16){
        if (string.length > 0)
        {

            centValue = centValue * 10 + [string intValue];
            double intermediate = [myNumber doubleValue] * 10 +  [[f numberFromString:string] doubleValue];
            
            
            
            result = [[NSNumber alloc] initWithDouble:intermediate];
        }
        else
        {
           
            centValue = centValue / 10;
            double intermediate = [myNumber doubleValue]/10;
            
          
            
            result = [[NSNumber alloc] initWithDouble:intermediate];
        }
        
        NSInteger am = [result intValue];
        NSInteger bal = [myNumberBal intValue];
        
        if(am > bal)
        {
            [self showAlertWithMessage:@"Por el momento no puedes solicitar este monto"];
            return NO;
        }
       
        
        myNumber = result;
         NSLog(@"%ld ++++ %@", (long)centValue, myNumber);
            NSNumber *formatedValue;
            formatedValue = [[NSNumber alloc] initWithDouble:[myNumber doubleValue]/ 100.0f];
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            textField.text = [_currencyFormatter stringFromNumber:formatedValue];
            [self calculatepercentage:textField.text];
            return NO;
    }else{

        NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
        [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        textField.text = [_currencyFormatter stringFromNumber:00];

        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Deposit Amount Limit"
                                                       message: @"You've exceeded the deposit amount limit. Kindly re-input amount"
                                                      delegate: self
                                             cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"OK",nil];

        [alert show];
        return NO;
    }
    }
    else
    {
        [self showAlertWithMessage:@"Por el momento no puedes solicitar este monto"];
        return NO;
    }
    return YES;
}


- (void)doneButtonDidPressed:(id)sender {
    [self.lbl_amount resignFirstResponder];
}

//+ (CGFloat)toolbarHeight {
//    // This method will handle the case that the height of toolbar may change in future iOS.
//    return 44.f;
//}

-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Family Doc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];

}
- (IBAction)btn_close:(id)sender {
    
     [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
